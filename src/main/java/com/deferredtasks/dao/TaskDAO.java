package com.deferredtasks.dao;

import com.deferredtasks.model.entities.Task;
import com.deferredtasks.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;

@Service
public class TaskDAO {

    @Autowired
    private TaskRepository taskRepository;

    public List<Task> findAllByExecutionDate(Instant executionDate) {
        return taskRepository.findAllByExecutionDate(executionDate);
    }

    public void save(Task task) {
        taskRepository.save(task);
    }

    public void deleteById(Long id) {
        taskRepository.deleteById(id);
    }
}
