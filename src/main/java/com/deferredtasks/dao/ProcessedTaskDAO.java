package com.deferredtasks.dao;

import com.deferredtasks.model.entities.ProcessedTask;
import com.deferredtasks.repository.ProcessedTaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;

@Service
public class ProcessedTaskDAO {

    @Autowired
    ProcessedTaskRepository processedTaskRepository;

    public void save(ProcessedTask processedTask) {
        processedTaskRepository.save(processedTask);
    }

    public void deleteAllByExecutionDateIsLessThan(Instant deletionDate) {
        processedTaskRepository.deleteAllByExecutionDateIsLessThan(deletionDate);
    }
}
