package com.deferredtasks.controller;

import com.deferredtasks.model.entities.PubSubTask;
import com.deferredtasks.model.entities.RestTask;
import com.deferredtasks.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TaskController {

    @Autowired
    private TaskService taskService;

    @PostMapping(value = "/restTasks")
    public void handleRestTask(@RequestBody RestTask task) {
        taskService.saveTask(task);
    }

    @PostMapping(value = "/pubsubTasks")
    public void handlePubSubTask(@RequestBody PubSubTask task) {
        taskService.saveTask(task);
    }
}

