package com.deferredtasks.repository;

import com.deferredtasks.model.entities.ProcessedTask;
import lombok.NonNull;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;

@Repository
public interface ProcessedTaskRepository extends CrudRepository<ProcessedTask, Long> {

    @NonNull
    List<ProcessedTask> findAll();

    void deleteAllByExecutionDateIsLessThan(Instant deletionDate);
}
