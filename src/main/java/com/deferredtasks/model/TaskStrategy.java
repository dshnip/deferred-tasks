package com.deferredtasks.model;

public enum TaskStrategy {
    REST, PUBSUB
}
