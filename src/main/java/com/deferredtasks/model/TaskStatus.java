package com.deferredtasks.model;

public enum TaskStatus {
    FAILED, COMPLETED
}
