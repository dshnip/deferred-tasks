package com.deferredtasks.model.entities;

import com.deferredtasks.model.TaskStatus;
import com.deferredtasks.model.TaskStrategy;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpMethod;

import javax.persistence.*;
import java.time.Instant;
import java.util.HashMap;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
public class ProcessedTask {
    //common part
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    private Instant executionDate;
    @Enumerated(EnumType.STRING)
    private TaskStrategy strategy;
    private HashMap<String, String> headers;
    private String payload;
    //rest
    private String url;
    @Enumerated(EnumType.STRING)
    private HttpMethod method;
    //pubsub
    private String topic;
    //message
    private String message;
    @Enumerated(EnumType.STRING)
    private TaskStatus taskStatus;


    public ProcessedTask(Instant executionDate, TaskStrategy strategy, HashMap<String, String> headers,
                         String payload, String url, HttpMethod method, String topic, String message,
                         TaskStatus taskStatus) {
        this.executionDate = executionDate;
        this.strategy = strategy;
        this.headers = headers;
        this.payload = payload;
        this.url = url;
        this.method = method;
        this.topic = topic;
        this.message = message;
        this.taskStatus = taskStatus;
    }
}
