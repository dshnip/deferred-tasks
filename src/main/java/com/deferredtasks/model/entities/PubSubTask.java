package com.deferredtasks.model.entities;

import com.deferredtasks.model.TaskStrategy;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.time.Instant;
import java.util.HashMap;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class PubSubTask extends Task {
    @Column(nullable = false)
    private String topic;

    {
        super.setStrategy(TaskStrategy.PUBSUB);
    }

    public PubSubTask(Instant executionDate, HashMap<String, String> headers, String payload, String topic) {
        super(executionDate, headers, payload);
        this.topic = topic;
    }

}
