package com.deferredtasks.model.entities;


import com.deferredtasks.model.TaskStrategy;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpMethod;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.time.Instant;
import java.util.HashMap;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class RestTask extends Task {
    @Column(nullable = false)
    private String url;
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private HttpMethod method;

    {
        super.setStrategy(TaskStrategy.REST);
    }

    public RestTask(Instant executionDate, HashMap<String, String> headers, String payload, String url, HttpMethod method) {
        super(executionDate, headers, payload);
        this.url = url;
        this.method = method;
    }
}
