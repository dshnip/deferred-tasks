package com.deferredtasks.model.entities;

import com.deferredtasks.model.TaskStrategy;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    private Instant executionDate;
    @Enumerated(EnumType.STRING)
    private TaskStrategy strategy;
    private HashMap<String, String> headers;
    private String payload;

    public Task(Instant executionDate, HashMap<String, String> headers, String payload) {
        this.executionDate = executionDate;
        this.headers = headers;
        this.payload = payload;
    }

    public void setExecutionDate(Instant executionDate) {
        executionDate = executionDate.truncatedTo(ChronoUnit.MINUTES);
        this.executionDate = executionDate;
    }
}
