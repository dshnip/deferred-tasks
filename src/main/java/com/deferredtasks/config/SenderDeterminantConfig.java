package com.deferredtasks.config;

import com.deferredtasks.model.entities.PubSubTask;
import com.deferredtasks.model.entities.RestTask;
import com.deferredtasks.model.entities.Task;
import com.deferredtasks.service.sender.common.SenderService;
import com.deferredtasks.service.sender.pubsub.PubSubSenderService;
import com.deferredtasks.service.sender.rest.RestSenderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;

@Configuration
public class SenderDeterminantConfig {

    @Autowired
    private ApplicationContext applicationContext;

    public HashMap<Class<? extends Task>, SenderService> configureSenderBeanMap(HashMap<Class<? extends Task>, SenderService> map) {
        map.put(RestTask.class, applicationContext.getBean(RestSenderService.class));
        map.put(PubSubTask.class, applicationContext.getBean(PubSubSenderService.class));
        return map;
    }
}
