package com.deferredtasks.config.datetime;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

public class InstantTimeBeanWrapper {

    public Instant currentTime() {
        return Instant.now().truncatedTo(ChronoUnit.MINUTES);
    }
}
