package com.deferredtasks.config.datetime;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class InstantTimeBeanConfig {

    @Bean
    public InstantTimeBeanWrapper localDateTimeBean() {
        return new InstantTimeBeanWrapper();
    }
}
