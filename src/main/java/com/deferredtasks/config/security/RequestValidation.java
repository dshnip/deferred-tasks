package com.deferredtasks.config.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class RequestValidation {

    @Value("${securityKey}")
    private String securityKey;

    public boolean validation(String authenticationKey) {
        return authenticationKey != null && authenticationKey.equals(securityKey);
    }
}
