package com.deferredtasks.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class RequestAuthenticationFilter extends OncePerRequestFilter {

    @Autowired
    private RequestValidation requestValidation;

    @Value("${securityHeader}")
    private String securityHeader;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {

        String authenticationKey = request.getHeader(securityHeader);

        if (!requestValidation.validation(authenticationKey)) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Incorrect security key");
            return;
        }

        filterChain.doFilter(request, response);
    }
}