package com.deferredtasks.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
public class FilterBean {

    @Autowired
    private RequestAuthenticationFilter requestAuthenticationFilter;

    @Bean
    public FilterRegistrationBean<RequestAuthenticationFilter> requestAuthenticationFilterBean() {
        FilterRegistrationBean<RequestAuthenticationFilter> filterRegistrationBean = new FilterRegistrationBean<>();
        filterRegistrationBean.setFilter(requestAuthenticationFilter);
        filterRegistrationBean.setUrlPatterns(List.of("/pubsubTasks/*", "/restTasks/*"));
        return filterRegistrationBean;
    }
}
