package com.deferredtasks.service;


import com.deferredtasks.dao.ProcessedTaskDAO;
import com.deferredtasks.model.entities.ProcessedTask;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;

@Slf4j
@Service
public class ProcessedTaskService {

    @Autowired
    private ProcessedTaskDAO processedTaskDAO;

    public void save(ProcessedTask processedTask) {
        processedTaskDAO.save(processedTask);
    }

    public void deleteAllByExecutionDateIsLessThan(Instant deletionDate) {
        processedTaskDAO.deleteAllByExecutionDateIsLessThan(deletionDate);
    }
}