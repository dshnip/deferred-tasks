package com.deferredtasks.service.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
public class PrepareMessageService {

    public String preparePayload(String encodedPayload) {
        if (encodedPayload == null)
            return "";
        byte[] decodeBytes = Base64.getDecoder().decode(encodedPayload);
        return new String(decodeBytes);
    }

    public void prepareHeaders(HttpHeaders httpHeaders, HashMap<String, String> mapHeaders) {
        if (mapHeaders == null)
            return;
        for (Map.Entry<String, String> header : mapHeaders.entrySet()) {
            httpHeaders.set(header.getKey(), header.getValue());
        }
    }

}
