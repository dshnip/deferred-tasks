package com.deferredtasks.service.util.converter;

import com.deferredtasks.model.entities.ProcessedTask;
import com.deferredtasks.model.entities.RestTask;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class RestTaskConverter extends AbstractTaskConverter<RestTask> {
    @Override
    protected void postConvert(RestTask restTask, ProcessedTask processedTask) {
        processedTask.setMethod(restTask.getMethod());
        processedTask.setUrl(restTask.getUrl());
    }
}
