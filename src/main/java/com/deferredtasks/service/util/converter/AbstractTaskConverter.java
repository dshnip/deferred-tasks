package com.deferredtasks.service.util.converter;

import com.deferredtasks.model.TaskStatus;
import com.deferredtasks.model.entities.ProcessedTask;
import com.deferredtasks.model.entities.Task;

public abstract class AbstractTaskConverter<T extends Task> implements TaskConverter<T> {
    @Override
    public ProcessedTask convert(T task, TaskStatus taskStatus) {
        ProcessedTask processedTask = new ProcessedTask();

        processedTask.setExecutionDate(task.getExecutionDate());
        processedTask.setStrategy(task.getStrategy());
        processedTask.setHeaders(task.getHeaders());
        processedTask.setPayload(task.getPayload());

        processedTask.setTaskStatus(taskStatus);

        postConvert(task, processedTask);

        return processedTask;
    }

    protected abstract void postConvert(T task, ProcessedTask processedTask);
}
