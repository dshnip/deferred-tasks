package com.deferredtasks.service.util.converter;

import com.deferredtasks.model.TaskStatus;
import com.deferredtasks.model.entities.ProcessedTask;
import com.deferredtasks.model.entities.Task;
import org.springframework.stereotype.Component;

@Component
public interface TaskConverter<T extends Task> {
    ProcessedTask convert(T task, TaskStatus taskStatus);
}
