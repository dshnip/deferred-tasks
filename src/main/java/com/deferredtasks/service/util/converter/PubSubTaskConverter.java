package com.deferredtasks.service.util.converter;

import com.deferredtasks.model.entities.ProcessedTask;
import com.deferredtasks.model.entities.PubSubTask;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class PubSubTaskConverter extends AbstractTaskConverter<PubSubTask> {
    @Override
    protected void postConvert(PubSubTask pubSubTask, ProcessedTask processedTask) {
        processedTask.setTopic(pubSubTask.getTopic());
    }
}
