package com.deferredtasks.service.scheduler;

import com.deferredtasks.config.datetime.InstantTimeBeanWrapper;
import com.deferredtasks.service.ProcessedTaskService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

@Slf4j
@Service
public class SchedulerCleanerService {

    @Autowired
    private ProcessedTaskService processedTaskService;

    @Autowired
    private InstantTimeBeanWrapper instantTimeBeanWrapper;

    private static final String CRON = "0 0 12 * * *"; // Every day, at noon

    @Transactional
    @Scheduled(cron = CRON)
    public void clean() {
        Instant deletionDate = instantTimeBeanWrapper.currentTime().minus(60, ChronoUnit.DAYS);
        processedTaskService.deleteAllByExecutionDateIsLessThan(deletionDate);
    }
}