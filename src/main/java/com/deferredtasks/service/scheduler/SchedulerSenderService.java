package com.deferredtasks.service.scheduler;

import com.deferredtasks.config.datetime.InstantTimeBeanWrapper;
import com.deferredtasks.model.entities.ProcessedTask;
import com.deferredtasks.model.entities.Task;
import com.deferredtasks.service.ProcessedTaskService;
import com.deferredtasks.service.TaskService;
import com.deferredtasks.service.sender.SenderDeterminant;
import com.deferredtasks.service.sender.common.SenderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Slf4j
@Service
public class SchedulerSenderService {

    @Autowired
    private TaskService taskService;

    @Autowired
    private ProcessedTaskService processedTaskService;

    @Autowired
    private InstantTimeBeanWrapper instantTimeBeanWrapper;

    @Autowired
    private SenderDeterminant senderDeterminant;

    private static final String CRON = "0 * * * * *"; // Every minute

    @Transactional
    @Scheduled(cron = CRON)
    public void send() {
        List<Task> taskList = taskService.findAllByExecutionDate(instantTimeBeanWrapper.currentTime());
        if (!taskList.isEmpty()) {
            taskList.forEach(task -> {
                SenderService senderService = senderDeterminant.getSenderBeanMap().get(task.getClass());
                senderService.send(task, this::onComplete);
            });
        }
    }

    public void onComplete(ProcessedTask processedTask, Task task) {
        processedTaskService.save(processedTask);
        taskService.deleteById(task.getId());
    }
}