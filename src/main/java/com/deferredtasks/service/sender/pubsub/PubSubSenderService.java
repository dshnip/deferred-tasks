package com.deferredtasks.service.sender.pubsub;

import com.deferredtasks.model.TaskStatus;
import com.deferredtasks.model.entities.ProcessedTask;
import com.deferredtasks.model.entities.PubSubTask;
import com.deferredtasks.service.sender.common.AbstractSenderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

@Slf4j
@Service
public class PubSubSenderService extends AbstractSenderService<PubSubTask> {

    @Autowired
    private PubSubSender pubSubSender;

    @Override
    public void send(PubSubTask pubSubTask, CompletionCallback completionCallback) {

        ListenableFuture<String> responseFuture = pubSubSender.sendPubSub(pubSubTask);

        responseFuture.addCallback(new ListenableFutureCallback<>() {
            @Override
            public void onSuccess(String result) {
                ProcessedTask processedTask = taskConverter.convert(pubSubTask, TaskStatus.COMPLETED);

                completionCallback.onComplete(processedTask, pubSubTask);
            }

            @Override
            public void onFailure(Throwable exception) {
                log.error("Task can't be executed. Time: {}, error: {}",
                        pubSubTask.getExecutionDate(),
                        exception.getMessage());

                ProcessedTask processedTask = taskConverter.convert(pubSubTask, TaskStatus.FAILED);
                processedTask.setMessage(exception.getMessage());

                completionCallback.onComplete(processedTask, pubSubTask);
            }
        });
    }
}