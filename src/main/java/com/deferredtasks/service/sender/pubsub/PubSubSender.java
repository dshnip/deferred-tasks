package com.deferredtasks.service.sender.pubsub;

import com.deferredtasks.model.entities.PubSubTask;
import com.deferredtasks.service.util.PrepareMessageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gcp.pubsub.core.PubSubTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;

@Slf4j
@Component
public class PubSubSender {

    @Autowired
    private PubSubTemplate pubSubTemplate;

    @Autowired
    private PrepareMessageService prepareMessageService;


    public ListenableFuture<String> sendPubSub(PubSubTask pubSubTask) {
        return
                pubSubTemplate.publish(pubSubTask.getTopic(),
                prepareMessageService.preparePayload(pubSubTask.getPayload()),
                pubSubTask.getHeaders());
    }
}