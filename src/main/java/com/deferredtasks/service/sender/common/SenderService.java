package com.deferredtasks.service.sender.common;

import com.deferredtasks.model.entities.ProcessedTask;
import com.deferredtasks.model.entities.Task;

public interface SenderService<T extends Task> {
    void send(T task, CompletionCallback completionCallback);

    interface CompletionCallback {
        void onComplete(ProcessedTask processedTask, Task task);
    }
}