package com.deferredtasks.service.sender.common;

import com.deferredtasks.model.entities.Task;
import com.deferredtasks.service.util.converter.TaskConverter;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbstractSenderService<T extends Task> implements SenderService<T> {
    @Autowired
    protected TaskConverter<T> taskConverter;
}