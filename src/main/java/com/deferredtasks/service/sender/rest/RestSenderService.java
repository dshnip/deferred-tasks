package com.deferredtasks.service.sender.rest;

import com.deferredtasks.model.TaskStatus;
import com.deferredtasks.model.entities.ProcessedTask;
import com.deferredtasks.model.entities.RestTask;
import com.deferredtasks.service.sender.common.AbstractSenderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class RestSenderService extends AbstractSenderService<RestTask> {

    @Autowired
    private RestSender restSender;

    @Override
    public void send(RestTask restTask, CompletionCallback completionCallback) {
        ProcessedTask processedTask;
        try {
            ResponseEntity<String> responseEntity = restSender.sendRest(restTask);

            processedTask = taskConverter.convert(restTask, TaskStatus.COMPLETED);
        } catch (RuntimeException exception) {
            log.error("Task can't be executed. Time: {}, error: {}",
                    restTask.getExecutionDate(),
                    exception.getMessage());

            processedTask = taskConverter.convert(restTask, TaskStatus.FAILED);
            processedTask.setMessage(exception.getMessage());
        }
        completionCallback.onComplete(processedTask, restTask);
    }
}