package com.deferredtasks.service.sender.rest;

import com.deferredtasks.model.entities.RestTask;
import com.deferredtasks.service.util.PrepareMessageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

@Slf4j
@Component
public class RestSender {

    @Autowired
    private PrepareMessageService prepareMessageService;

    private final WebClient webClient;

    public RestSender(WebClient.Builder webClientBuilder) {
        this.webClient = webClientBuilder.build();
    }

    public ResponseEntity<String> sendRest(RestTask restTask) {
        return webClient
                .method(restTask.getMethod())
                .uri(restTask.getUrl())
                .headers(httpHeaders -> prepareMessageService.prepareHeaders(httpHeaders, restTask.getHeaders()))
                .bodyValue(prepareMessageService.preparePayload(restTask.getPayload()))
                .retrieve()
                .toEntity(String.class)
                .block();
    }
}
