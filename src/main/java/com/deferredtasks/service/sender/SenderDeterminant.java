package com.deferredtasks.service.sender;

import com.deferredtasks.config.SenderDeterminantConfig;
import com.deferredtasks.model.entities.Task;
import com.deferredtasks.service.sender.common.SenderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component
public class SenderDeterminant {

    @Autowired
    private SenderDeterminantConfig senderDeterminantConfig;

    public HashMap<Class<? extends Task>, SenderService> getSenderBeanMap() {
        HashMap<Class<? extends Task>, SenderService> map = new HashMap<>();
        return senderDeterminantConfig.configureSenderBeanMap(map);
    }
}