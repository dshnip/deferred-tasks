package com.deferredtasks.service;

import com.deferredtasks.config.datetime.InstantTimeBeanWrapper;
import com.deferredtasks.dao.TaskDAO;
import com.deferredtasks.exception.OutOfTimeException;
import com.deferredtasks.model.entities.Task;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;

@Slf4j
@Service
public class TaskService {

    @Autowired
    private TaskDAO taskDAO;

    @Autowired
    private InstantTimeBeanWrapper instantTimeBeanWrapper;

    public void setInstantTimeBeanWrapper(InstantTimeBeanWrapper instantTimeBeanWrapper) {
        this.instantTimeBeanWrapper = instantTimeBeanWrapper;
    }

    public InstantTimeBeanWrapper getInstantTimeBeanWrapper() {
        return instantTimeBeanWrapper;
    }

    public void saveTask(Task task) {
        try {
            if (task.getExecutionDate().isBefore(getInstantTimeBeanWrapper().currentTime()) ||
                    task.getExecutionDate().equals(getInstantTimeBeanWrapper().currentTime())) {
                throw new OutOfTimeException("Execution time have passed");
            }
        } catch (OutOfTimeException exception) {
            log.error("Task can't be saved." +
                    " Error: {}", exception.getMessage());
            throw exception;
        }
        taskDAO.save(task);
    }

    public List<Task> findAllByExecutionDate(Instant executionDate) {
        return taskDAO.findAllByExecutionDate(executionDate);
    }

    public void deleteById(Long id) {
        taskDAO.deleteById(id);
    }
}