package com.deferredtasks.exception;

import com.deferredtasks.config.datetime.InstantTimeBeanWrapper;
import com.deferredtasks.model.entities.RestTask;
import com.deferredtasks.service.TaskService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ActiveProfiles("test")
@SpringBootTest
public class OutOfTimeExceptionTest {

    @Autowired
    private TaskService taskService;

    @Autowired
    private InstantTimeBeanWrapper instantTimeBeanWrapper;

    @Test
    public void testSaveTaskCurrentTime() {
        RestTask task = new RestTask(instantTimeBeanWrapper.currentTime(), null,
                null, "https://www.google.com", HttpMethod.GET);

        Exception currentTimeException = assertThrows(OutOfTimeException.class, () -> taskService.saveTask(task));

        String expectedMessage = "Execution time have passed";
        String currentTimeExceptionMessage = currentTimeException.getMessage();

        assertTrue(currentTimeExceptionMessage.contains(expectedMessage));
    }

    @Test
    public void testSaveTaskPassedTime() {
        RestTask task = new RestTask(instantTimeBeanWrapper.currentTime(), null,
                null, "https://www.google.com", HttpMethod.GET);

        Exception passedTimeException = assertThrows(OutOfTimeException.class, () -> taskService.saveTask(task));

        String expectedMessage = "Execution time have passed";
        String passedTimeExceptionMessage = passedTimeException.getMessage();

        assertTrue(passedTimeExceptionMessage.contains(expectedMessage));
    }
}