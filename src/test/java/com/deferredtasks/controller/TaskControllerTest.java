package com.deferredtasks.controller;

import com.deferredtasks.config.datetime.InstantTimeBeanWrapper;
import com.deferredtasks.model.entities.RestTask;
import com.deferredtasks.service.TaskService;
import com.deferredtasks.service.util.converter.TaskConverter;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import java.time.Instant;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class TaskControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private TaskController taskController;

    @Autowired
    private TaskService taskService;

    @Autowired
    private InstantTimeBeanWrapper instantTimeBeanWrapper;

    @BeforeEach
    public void init() {
        InstantTimeBeanWrapper timeMock = mock(InstantTimeBeanWrapper.class);
        Mockito.when(timeMock.currentTime()).thenReturn(Instant.now().minusSeconds(60));
        taskService.setInstantTimeBeanWrapper(timeMock);
    }

    @AfterEach
    public void destroy() {
        taskService.setInstantTimeBeanWrapper(instantTimeBeanWrapper);
    }

    @Test
    public void testHandleIncorrectSecurityKey() throws Exception {
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        String payload = "Im5hbWVzIjogWwogICAgICAgICJEb3JvdGhlYSIsICJSb3NlIiwgIkJsYW5jaGUiLCAiU29waGlhIgpd";
        RestTask task = new RestTask(instantTimeBeanWrapper.currentTime(), headers,
                payload, "postman-echo.com/post", HttpMethod.POST);

        MvcResult result = this.mockMvc.perform(post("/restTasks").header("Security-Key", "123456")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(task)))
                .andExpect(status().isUnauthorized())
                .andReturn();

        String expectedMessage = "Incorrect security key";
        String actualMessage = result.getResponse().getErrorMessage();

        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    public void testHandleNullSecurityKey() throws Exception {
        RestTask task = new RestTask(instantTimeBeanWrapper.currentTime(), null,
                null, "postman-echo.com/get", HttpMethod.GET);

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        JavaTimeModule module = new JavaTimeModule();
        objectMapper.registerModule(module);

        MvcResult result = this.mockMvc.perform(post("/restTasks")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(task)))
                .andExpect(status().isUnauthorized())
                .andReturn();

        String expectedMessage = "Incorrect security key";
        String actualMessage = result.getResponse().getErrorMessage();

        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    public void testHandleCorrectSecurityKey() throws Exception {
        RestTask task = new RestTask(instantTimeBeanWrapper.currentTime(), null,
                null, "postman-echo.com/get", HttpMethod.GET);

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        JavaTimeModule module = new JavaTimeModule();
        objectMapper.registerModule(module);

        this.mockMvc.perform(post("/restTasks").header("Security-Key", "12345")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(task)).characterEncoding("utf-8"))
                .andExpect(status().isOk());
    }

    @Test
    public void testController() {
        assertNotNull(taskController);
    }
}