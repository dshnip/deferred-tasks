package com.deferredtasks.service.sender;

import com.deferredtasks.config.datetime.InstantTimeBeanWrapper;
import com.deferredtasks.controller.TaskController;
import com.deferredtasks.model.TaskStatus;
import com.deferredtasks.model.entities.ProcessedTask;
import com.deferredtasks.model.entities.RestTask;
import com.deferredtasks.service.scheduler.SchedulerSenderService;
import com.deferredtasks.service.TaskService;
import com.deferredtasks.service.sender.rest.RestSenderService;
import com.deferredtasks.service.util.converter.TaskConverter;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.ActiveProfiles;

import java.time.Instant;
import java.util.HashMap;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ActiveProfiles("test")
@SpringBootTest
public class RestSenderServiceTest {

    @Autowired
    private TaskService taskService;

    @Autowired
    private SchedulerSenderService schedulerSenderService;

    @Autowired
    private InstantTimeBeanWrapper instantTimeBeanWrapper;

    @Autowired
    private TaskController taskController;

    @Mock
    private TaskConverter<RestTask> taskConverterMock;

    @Autowired
    @InjectMocks
    private RestSenderService restSenderService;

    @Mock
    private RestSenderService restSenderServiceMock;

    @BeforeEach
    public void init() {
        InstantTimeBeanWrapper timeMock = Mockito.mock(InstantTimeBeanWrapper.class);
        when(timeMock.currentTime()).thenReturn(Instant.now().minusSeconds(60));
        taskService.setInstantTimeBeanWrapper(timeMock);
    }

    @AfterEach
    public void destroy() {
        taskService.setInstantTimeBeanWrapper(instantTimeBeanWrapper);
    }

    @Test
    public void testSendRestTask() {
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        String payload = "Im5hbWVzIjogWwogICAgICAgICJEb3JvdGhlYSIsICJSb3NlIiwgIkJsYW5jaGUiLCAiU29waGlhIgpd";
        RestTask task = new RestTask(instantTimeBeanWrapper.currentTime(), headers,
                payload, "postman-echo.com/post", HttpMethod.POST);

        MockitoAnnotations.openMocks(this);

        ProcessedTask processedTask = new ProcessedTask();
        processedTask.setExecutionDate(task.getExecutionDate());
        processedTask.setTaskStatus(TaskStatus.COMPLETED);
        when(taskConverterMock.convert(any(), any())).thenReturn(processedTask);

        taskController.handleRestTask(task);
        schedulerSenderService.send();

        verify(taskConverterMock, times(1)).convert(any(), eq(TaskStatus.COMPLETED));
    }

    @Test
    public void testSendEmptyPayloadRestTask() {
        RestTask task = new RestTask(instantTimeBeanWrapper.currentTime(), null,
                null, "https://www.google.com", HttpMethod.GET);

        taskController.handleRestTask(task);
        schedulerSenderService.send();
    }

    @Test
    public void testIncorrectRestTask() {
        RestTask task = new RestTask(instantTimeBeanWrapper.currentTime(), null,
                null, "postman-echo.com/random_url", HttpMethod.POST);

        doThrow(new RuntimeException("404 Not Found")).when(restSenderServiceMock).send(any(), any());

        taskService.saveTask(task);
        schedulerSenderService.send();
    }
}