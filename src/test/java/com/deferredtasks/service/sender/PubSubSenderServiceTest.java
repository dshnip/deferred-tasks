package com.deferredtasks.service.sender;

import com.deferredtasks.config.datetime.InstantTimeBeanWrapper;
import com.deferredtasks.controller.TaskController;
import com.deferredtasks.model.entities.PubSubTask;
import com.deferredtasks.service.TaskService;
import com.deferredtasks.service.sender.common.SenderService;
import com.deferredtasks.service.sender.pubsub.PubSubSender;
import com.deferredtasks.service.sender.pubsub.PubSubSenderService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import java.time.Instant;
import java.util.HashMap;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

@ActiveProfiles("test")
@SpringBootTest
public class PubSubSenderServiceTest {

    @Autowired
    private InstantTimeBeanWrapper instantTimeBeanWrapper;

    @Autowired
    private TaskService taskService;

    @Autowired
    private PubSubSenderService pubSubSenderService;

    @Autowired
    private TaskController taskController;

    @Mock
    private PubSubSender pubSubSenderMock;

    @Mock
    private SenderService.CompletionCallback completionCallbackMock;

    @BeforeEach
    public void init() {
        InstantTimeBeanWrapper timeMock = Mockito.mock(InstantTimeBeanWrapper.class);
        when(timeMock.currentTime()).thenReturn(Instant.now().minusSeconds(60));
        taskService.setInstantTimeBeanWrapper(timeMock);
    }

    @AfterEach
    public void destroy() {
        taskService.setInstantTimeBeanWrapper(instantTimeBeanWrapper);
    }

    @Test
    public void testSendPubSubTask() {
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        String payload = "VGhpcyBpcyB0ZXN0IG1lc3NhZ2U=";
        PubSubTask task = new PubSubTask(instantTimeBeanWrapper.currentTime(), headers,
                payload, "deferred-tasks-topic-test");

        taskController.handlePubSubTask(task);

        ListenableFuture<String> responseFuture = mock(ListenableFuture.class);
        String result = "1111111";
        when(pubSubSenderMock.sendPubSub(task)).thenReturn(responseFuture);
        doAnswer(
                invocation -> {
                    ListenableFutureCallback<String> listenableFutureCallback = invocation.getArgument(0);
                    listenableFutureCallback.onSuccess(result);
                    return null;
                }
        ).when(responseFuture).addCallback(any(ListenableFutureCallback.class));
        pubSubSenderService.send(task, completionCallbackMock);
    }

    @Test
    public void testIncorrectPubSubTask() {
        PubSubTask task = new PubSubTask(instantTimeBeanWrapper.currentTime(), null,
                null, "impossible-topic-12345-xxxxx");

        ListenableFuture<String> responseFuture = mock(ListenableFuture.class);
        Throwable throwable = mock(Throwable.class);
        given(throwable.getMessage()).willReturn("error");
        when(pubSubSenderMock.sendPubSub(task)).thenReturn(responseFuture);
        doAnswer(
                invocation -> {
                    ListenableFutureCallback<String> listenableFutureCallback = invocation.getArgument(0);
                    listenableFutureCallback.onFailure(throwable);
                    return null;
                }
        ).when(responseFuture).addCallback(any(ListenableFutureCallback.class));
        pubSubSenderService.send(task, completionCallbackMock);
    }
}