package com.deferredtasks.service.scheduler;

import com.deferredtasks.config.datetime.InstantTimeBeanWrapper;
import com.deferredtasks.dao.ProcessedTaskDAO;
import com.deferredtasks.model.entities.PubSubTask;
import com.deferredtasks.repository.ProcessedTaskRepository;
import com.deferredtasks.service.ProcessedTaskService;
import com.deferredtasks.service.TaskService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ActiveProfiles("test")
@SpringBootTest
class SchedulerCleanerServiceTest {

    @Autowired
    private SchedulerCleanerService schedulerCleanerService;

    @Autowired
    @InjectMocks
    private ProcessedTaskService processedTaskService;

    @Mock
    private ProcessedTaskDAO processedTaskDAO;

    @Mock
    private ProcessedTaskRepository processedTaskRepository;

    @Autowired
    private InstantTimeBeanWrapper instantTimeBeanWrapper;

    @Autowired
    private TaskService taskService;

    @BeforeEach
    public void init() {
        InstantTimeBeanWrapper timeMock = Mockito.mock(InstantTimeBeanWrapper.class);
        when(timeMock.currentTime()).thenReturn(Instant.now().minus(80, ChronoUnit.DAYS));
        taskService.setInstantTimeBeanWrapper(timeMock);
    }

    @AfterEach
    public void destroy() {
        taskService.setInstantTimeBeanWrapper(instantTimeBeanWrapper);
    }

    @Test
    public void testCleanMethod() {
        PubSubTask pubSubTask = new PubSubTask(instantTimeBeanWrapper.currentTime().minus(70, ChronoUnit.DAYS), null,
                null, "test-topic");

        MockitoAnnotations.openMocks(this);

        doNothing().when(processedTaskRepository).deleteAllByExecutionDateIsLessThan(any());

        taskService.saveTask(pubSubTask);
        schedulerCleanerService.clean();

        Mockito.verify(processedTaskDAO, times(1)).deleteAllByExecutionDateIsLessThan(any());
    }

    @Test
    public void testCleanMethod2() {
        PubSubTask pubSubTask = new PubSubTask(instantTimeBeanWrapper.currentTime().minus(75, ChronoUnit.DAYS), null,
                null, "test-topic");
        taskService.saveTask(pubSubTask);
        schedulerCleanerService.clean();
    }
}