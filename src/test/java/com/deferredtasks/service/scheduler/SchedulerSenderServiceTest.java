package com.deferredtasks.service.scheduler;

import com.deferredtasks.config.datetime.InstantTimeBeanWrapper;
import com.deferredtasks.model.entities.PubSubTask;
import com.deferredtasks.model.entities.RestTask;
import com.deferredtasks.model.entities.Task;
import com.deferredtasks.service.TaskService;
import com.deferredtasks.service.scheduler.SchedulerSenderService;
import com.deferredtasks.service.sender.common.SenderService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.ActiveProfiles;

import java.time.Instant;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.when;

@ActiveProfiles("test")
@SpringBootTest
public class SchedulerSenderServiceTest {

    @Autowired
    private TaskService taskService;

    @Autowired
    private SchedulerSenderService schedulerSenderService;

    @Autowired
    private InstantTimeBeanWrapper instantTimeBeanWrapper;

    @Mock
    private SenderService<Task> senderServiceMock;

    @Mock
    private SenderService.CompletionCallback completionCallbackMock;

    @BeforeEach
    public void init() {
        InstantTimeBeanWrapper timeMock = Mockito.mock(InstantTimeBeanWrapper.class);
        when(timeMock.currentTime()).thenReturn(Instant.now().minusSeconds(60));
        taskService.setInstantTimeBeanWrapper(timeMock);
    }

    @AfterEach
    public void destroy() {
        taskService.setInstantTimeBeanWrapper(instantTimeBeanWrapper);
    }

    @Test
    public void testEmptyList() {
        RestTask restTask = new RestTask(instantTimeBeanWrapper.currentTime().plusSeconds(300), null,
                null, "postman-echo.com/random_url", HttpMethod.POST);
        PubSubTask pubSubTask = new PubSubTask(instantTimeBeanWrapper.currentTime().plusSeconds(300), null,
                null, "deferred-tasks-topic-test");

        taskService.saveTask(restTask);
        taskService.saveTask(pubSubTask);
        schedulerSenderService.send();

        Mockito.verify(senderServiceMock, never()).send(restTask, completionCallbackMock);
        Mockito.verify(senderServiceMock, never()).send(pubSubTask, completionCallbackMock);
    }
}