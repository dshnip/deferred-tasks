FROM adoptopenjdk/openjdk11:alpine
ADD build/libs/deferred-tasks-0.0.1-SNAPSHOT.jar deferred-tasks-app.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "deferred-tasks-app.jar"]
