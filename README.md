# Table of contents

- [About service](#about-service)
- [Getting started](#getting-started)
    - [Prerequisites](#prerequisites)
- [1. Create Docker Image](#1-create-docker-image)
- [2. Push the Docker Image to the Container Registry](#2-push-the-docker-image-to-the-container-registry)
- [3. Run kubernetes config files](#3-run-kubernetes-config-files)

# About service

# Getting started

## Prerequisites

- Java 11
- PostgreSQL
- Docker
- gcloud

# 1. Create Docker Image

Execute script below to perform the build task:

```
$ ./gradlew clean build
```

The following command will create a Docker image on your machine:

```
$ docker build -t gcr.io/severn-stage-1/deferred-tasks-app:{$VERSION} .
```

After executing above command, it will take few minutes to create a docker image.

Then, execute the below command to view the docker images:

```
$ docker images
```

# 2. Push the Docker Image to the Container Registry

Log in to the Google Cloud Console and go the Kubernetes Clusters page.
Click the connect button and get the command to connect the cluster and execute the command into your gcloud sdk:

```
$ gcloud container clusters get-credentials deferred-tasks-cluster --zone us-central1-c --project severn-stage-1
```

After executing the above command, you can see the below messages on the console:

```
Fetching cluster endpoint and auth data.
kubeconfig entry generated for deferred-tasks-cluster.
```

Execute following command and get the Short-lived access token for accessing the Google cloud instances:

```
$ gcloud docker -a
```

Then execute the below command on Google SDK to push the docker image into the Container Registry:

```
$ docker push gcr.io/severn-stage-1/deferred-tasks-app:{$VERSION}
```

Pushing docker image to container registry will take few minutes. After pushing, you can see the docker image 
on the container registry in your Google cloud console.

# 3. Run kubernetes config files

Kubernetes config files are located in the [k8s](k8s) folder.

Commands to configure secrets in the cluster:

```
$ kubectl apply -f k8s/serern-stage-1-secret.yml
```

```
$ kubectl apply -f k8s/secret.yml
```

Command to configure deployment and service in the cluster:

```
$ kubectl apply -f k8s/deployment.yml
```

To view, the deployments execute the command:

```
$ kubectl get deployments
```

To view, the list of pods running on the Kubernetes clusters:

```
$ kubectl get pods
```

To view the list of services running:

```
$ kubectl get services
```

Command to configure ingress in the cluster:

```
$ kubectl apply -f k8s/ingress.yml
```

To view the state of the Ingress:

```
$ kubectl get ing
```